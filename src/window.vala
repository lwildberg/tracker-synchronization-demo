/* window.vala
 *
 * Copyright 2023 Lorenz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Trackersyncrecieve {
    [GtkTemplate (ui = "/org/Example/TrackerSyncRecieve/window.ui")]
    public class Window : Adw.ApplicationWindow {
        [GtkChild]
        private unowned Gtk.Label label;
		[GtkChild]
		private unowned Gtk.Entry dbus_address;
 		[GtkChild]
 		private unowned Gtk.Entry data_entry;

		private Manager manager;
 		private Syncer syncer;
 		private Provider provider;
 		private Tracker.Sparql.Connection db;

        public Window (Gtk.Application app) {
            Object (application: app);
        }

		public async void init () {
			this.manager = yield new Manager ();
			this.db = this.manager.db;
			this.syncer = new Syncer (this.db);
			this.provider = new Provider (this.db);
			this.title = this.provider.get_address ();
		}

		[GtkCallback]
		public void on_sync_button () {
			this.syncer.synchronize.begin ((obj, res) => {
				this.syncer.synchronize.end (res);
				print ("synced\n");
			});
		}

		[GtkCallback]
		public void on_connect_button () {
			this.syncer.connect.begin (this.dbus_address.buffer.text, (obj, res) => {
				this.syncer.connect.end (res);
				print ("connected\n");
			});
		}

		[GtkCallback]
		public void on_load_button () {
			this.data_entry.buffer.set_text (manager.get_data ().data);
		}

		[GtkCallback]
		public void on_store_button () {
			this.manager.set_data (this.data_entry.buffer.get_text ());
		}
    }
}
