/* application.vala
 *
 * Copyright 2023 Lorenz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Trackersyncrecieve {
	public class Syncer : Object {
		public Tracker.Sparql.Connection local_db { get; construct; }
		private Tracker.Sparql.Connection remote_db;
		private Emergence.Sync sync;
		private DBusConnection dbus_connection;

		public Syncer (Tracker.Sparql.Connection local_db) {
			Object (local_db: local_db);
		}

		construct {
			this.dbus_connection = GLib.Application.get_default ().get_dbus_connection ();
			assert (this.dbus_connection != null);
		}

		public new async void connect (string service_name) {
			this.remote_db = yield Tracker.Sparql.Connection.bus_new_async (service_name, Provider.object_path);
			this.sync = new Emergence.Sync (this.local_db, @"dbus:session:$(this.dbus_connection.unique_name):$(Provider.object_path)", this.remote_db);
		}

		public async bool synchronize () {
			return yield this.sync.sync ();
		}
	}
}
