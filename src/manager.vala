/* application.vala
 *
 * Copyright 2023 Lorenz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Trackersyncrecieve {
	public class Manager : Object {
		public Tracker.Sparql.Connection db { get; private set; }

		public async Manager () {
			yield this.init ();
		}

		public async void init () {
			this.db = yield Tracker.Sparql.Connection.new_async (
				Tracker.Sparql.ConnectionFlags.NONE, null,
				File.new_for_uri ("resource:///org/Example/TrackerSyncRecieve")
			);
			this.db.update (Manager.setup);
		}

		private const string setup = """
		INSERT DATA {
			sy:default a sy:Text ;
				sy:content 'test' .
		}
		""";
		private const string getter = """
		SELECT ?object {
			?subject ?predicate ?object
		}
		""";
		private const string setter = """
		DELETE {
			?subject sy:content ?content
		} INSERT {
			?subject sy:content '%s'
		} WHERE {
			?subject a sy:Text ;
				sy:content ?content
		}
		""";

		public new string get_data () {
			print ("start\n");
			var cursor = this.db.query (Manager.getter);
			print ("start2\n");
			print (cursor.get_n_columns ().to_string () + "\n");
			cursor.next ();
			print (@"The value is: $(cursor.get_string (0))\n");
			return cursor.get_string (0).dup ();
		}

		public new void set_data (string value) {
			this.db.update (Manager.setter.printf (value));
		}
	}
}
