/* application.vala
 *
 * Copyright 2023 Lorenz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Trackersyncrecieve {
	public class Provider : Object {
		public Tracker.Sparql.Connection db { get; construct; }
		private DBusConnection dbus_connection;
		private Tracker.EndpointDBus endpoint;

		public const string object_path = "/trackersyncrecieve/endpoint";

		public Provider (Tracker.Sparql.Connection db) {
			Object (db: db);
		}

		construct {
			this.dbus_connection = GLib.Application.get_default ().get_dbus_connection ();
			assert (this.dbus_connection != null);
			this.endpoint = new Tracker.EndpointDBus (this.db, this.dbus_connection, Provider.object_path);
		}

		public string get_address () {
			return this.dbus_connection.unique_name;
		}
	}
}
