[CCode (cprefix="Tracker", lower_case_cprefix="tracker_", cheader_filename="emergence/tracker-sync.h")]
namespace Emergence {
	public sealed class Sync : GLib.Object {
		public Sync (Tracker.Sparql.Connection local_conn, string remote_uri, Tracker.Sparql.Connection remote_conn);
		public async bool sync (GLib.Cancellable? cancellable = null) throws GLib.Error;
	}
}
